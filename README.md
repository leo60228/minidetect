# MiniDetect
A Python script that, given a picture of a tablet screen (`in.png`) with objects on top of it, and an image of what's being displayed on the tablet (`in2.png`, will produce the tablet's *actual* screen (`screen.png`), as well as a version with all circular **objects** (circles on the background image are ignored!) of an eyeballed size of about an inch diameter highlighted.

## ROADMAP
* Detect median color of an outer ring on detected objects
* Port to C++
* Optimize until running takes less than ~8 seconds
* Run in "real-time," using a webcam, VNC server, and video capture script running on the VNC server's X display
* Design a mount to physically "attach" my webcam to the tablet, and hook up the tablet via USB-OTG and Ethernet
* Use a Roll20 API script and [Rollbot](https://gitlab.com/leo60228/rollbot) to mirror detected objects as Roll20 tokens (this is the end-goal of the project - an AR TTRPG battlemap!)
